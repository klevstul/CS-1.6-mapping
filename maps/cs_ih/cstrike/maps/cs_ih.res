// C:\Program Files\Counter-Strike\cstrike\maps\cs_ih.res - created with RESGen v1.10
// RESGen is made by Jeroen "ShadowLord" Bogers
// URL: http://www.unitedadmins.com/mapRESGEN.asp
// E-MAIL: resgen@hltools.com
// Res creation date, GMT timezone (dd-mm-yyyy): 31-10-2003

// .res entries:
cs_ih.wad
gfx/env/cs_ihup.tga
gfx/env/cs_ihdn.tga
gfx/env/cs_ihlf.tga
gfx/env/cs_ihrt.tga
gfx/env/cs_ihft.tga
gfx/env/cs_ihbk.tga
models/hostage.mdl
models/glassgibs.mdl
models/woodgibs.mdl
models/metalgibs.mdl
models/rockgibs.mdl
