// C:\Program Files\Counter-Strike\cstrike\maps\de_riphard.res - created with RESGen v1.10
// RESGen is made by Jeroen "ShadowLord" Bogers
// URL: http://www.unitedadmins.com/mapRESGEN.asp
// E-MAIL: resgen@hltools.com
// Res creation date, GMT timezone (dd-mm-yyyy): 09-01-2003

// .res entries:
halflife.wad
decals.wad
liquids.wad
de_riphard.wad
gfx/env/cliffup.tga
gfx/env/cliffdn.tga
gfx/env/clifflf.tga
gfx/env/cliffrt.tga
gfx/env/cliffft.tga
gfx/env/cliffbk.tga
models/metalgibs.mdl
sound/ambience/waterrun.wav
sound/ambience/wind1.wav
models/woodgibs.mdl
models/glassgibs.mdl
